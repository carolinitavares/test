//
//  GameScene.swift
//  SlimeZerkTest
//
//  Created by Parrot on 2019-02-25.
//  Copyright © 2019 Parrot. All rights reserved.
//

import SpriteKit
import GameplayKit

struct CollisionCategories{
    static let Enemy : UInt32 = 0x1 << 0
    static let Player: UInt32 = 0x1 << 1
    static let PlayerBullet: UInt32 = 0x1 << 2
    static let wall: UInt32 = 0x1 << 3
    static let otto: UInt32 = 0x1 << 4
}

class GameScene: SKScene, SKPhysicsContactDelegate {
    // MARK: Variables for tracking time
    private var lastUpdateTime : TimeInterval = 0
    
    // MARK: Sprite variables
    var player:SKSpriteNode = SKSpriteNode()
    var enemy:SKSpriteNode = SKSpriteNode()
    var enemy2:SKSpriteNode = SKSpriteNode()
    var enemy3:SKSpriteNode = SKSpriteNode()
    var upButton:SKSpriteNode = SKSpriteNode()
    var downButton:SKSpriteNode = SKSpriteNode()
    var backgroundMusic:SKAudioNode = SKAudioNode()
    var musicOn = true
    var exit:SKSpriteNode = SKSpriteNode()
    var scoreScreen:SKLabelNode = SKLabelNode()
    var livesScreen:SKLabelNode = SKLabelNode()
    var wall1:SKSpriteNode = SKSpriteNode()
    var wall2:SKSpriteNode = SKSpriteNode()
    var musicBtn: SKSpriteNode = SKSpriteNode()
    var otto:SKSpriteNode = SKSpriteNode()
    
    var ottoYes = false
    
    // MARK: make girl move
    var xn1:CGFloat = 0
    var yn1:CGFloat = 0
    var xn2:CGFloat = 0
    var yn2:CGFloat = 0
    var xn3:CGFloat = 0
    var yn3:CGFloat = 0
    var xnOtto:CGFloat = 0
    var ynOtto:CGFloat = 0
    
    // MARK: Label variables
    var livesLabel:SKLabelNode = SKLabelNode(text:"")
    
    
    // MARK: Scoring and Lives variables
    var score = 0
    var lives = 3
    
    // MARK: Game state variables
    let SPEED:CGFloat = 45
    let ENEMY_SPEED:CGFloat = 0.5
    
    // MARK: VARAIBLES TO TRACK GAME STATE
    var levelComplete = false
    var currentLevel = 1
    var orange:Orange?
    var mouseStartingPosition:CGPoint = CGPoint(x:0, y:0)
    var hit = false
    var goingLeft = false
    
    // MARK: Default GameScene functions
    // -------------------------------------
    override func didMove(to view: SKView) {
        self.lastUpdateTime = 0
        
        self.physicsWorld.contactDelegate = self
        
        //Music
        if let musicURL = Bundle.main.url(forResource: "main-theme02", withExtension: "m4a") {
            backgroundMusic = SKAudioNode(url: musicURL)
            addChild(backgroundMusic)
        }
        
        // get sprites from Scene Editor
        self.player = self.childNode(withName: "player") as! SKSpriteNode
        self.enemy = self.childNode(withName: "enemy") as! SKSpriteNode
        self.enemy2 = self.childNode(withName: "enemy2") as! SKSpriteNode
        if (currentLevel > 1){
            self.enemy3 = self.childNode(withName: "enemy3") as! SKSpriteNode
        }
        self.upButton = self.childNode(withName: "upButton") as! SKSpriteNode
        self.downButton = self.childNode(withName: "downButton") as! SKSpriteNode
        self.exit = self.childNode(withName: "exit") as! SKSpriteNode
        self.scoreScreen = self.childNode(withName: "score") as! SKLabelNode
        self.livesScreen = self.childNode(withName: "lives") as! SKLabelNode
        
        self.wall1 = self.childNode(withName: "wall1") as! SKSpriteNode
        self.wall2 = self.childNode(withName: "wall2") as! SKSpriteNode
        
        self.otto = self.childNode(withName: "otto") as! SKSpriteNode
        
        self.musicBtn = self.childNode(withName: "musicButton") as! SKSpriteNode
        // get labels from Scene Editor
        //self.livesLabel = self.childNode(withName: "livesLabel") as! SKLabelNode
        
        // hitboxes
        self.player.physicsBody = SKPhysicsBody(rectangleOf: CGSize(width: self.player.frame.width, height: self.player.frame.height))
        self.player.physicsBody?.affectedByGravity = false
        self.player.physicsBody?.isDynamic = true
        self.player.physicsBody?.categoryBitMask = CollisionCategories.Player
        //self.player.physicsBody?.collisionBitMask = CollisionCategories.Enemy + CollisionCategories.otto
        //self.player.physicsBody?.collisionBitMask = CollisionCategories.Enemy + CollisionCategories.otto
        
        self.enemy.physicsBody = SKPhysicsBody(rectangleOf: CGSize(width: self.enemy.frame.width, height: self.enemy.frame.height))
        self.enemy.physicsBody?.affectedByGravity = false
        self.enemy.physicsBody?.isDynamic = true
        
        self.enemy2.physicsBody = SKPhysicsBody(rectangleOf: CGSize(width: self.enemy2.frame.width, height: self.enemy2.frame.height))
        self.enemy2.physicsBody?.affectedByGravity = false
        self.enemy2.physicsBody?.isDynamic = true
        
        if (currentLevel > 1){
            self.enemy3.physicsBody = SKPhysicsBody(rectangleOf: CGSize(width: self.enemy3.frame.width, height: self.enemy3.frame.height))
            self.enemy3.physicsBody?.affectedByGravity = false
            self.enemy3.physicsBody?.isDynamic = true
        }
        
        /*self.exit.physicsBody = SKPhysicsBody(rectangleOf: CGSize(width: self.exit.frame.width, height: self.exit.frame.height))
        self.exit.physicsBody?.affectedByGravity = false
        self.exit.physicsBody?.isDynamic = false*/
        
        self.wall1.physicsBody = SKPhysicsBody(rectangleOf: CGSize(width: self.wall1.frame.width, height: self.wall1.frame.height))
        self.wall1.physicsBody?.affectedByGravity = false
        self.wall1.physicsBody?.isDynamic = false
        self.wall1.physicsBody?.categoryBitMask = CollisionCategories.wall
        
        self.wall2.physicsBody = SKPhysicsBody(rectangleOf: CGSize(width: self.wall2.frame.width, height: self.wall2.frame.height))
        self.wall2.physicsBody?.affectedByGravity = false
        self.wall2.physicsBody?.isDynamic = false
        self.wall2.physicsBody?.categoryBitMask = CollisionCategories.wall
        
        self.otto.physicsBody = SKPhysicsBody(rectangleOf: CGSize(width: self.otto.frame.width, height: self.otto.frame.height))
        self.otto.physicsBody?.affectedByGravity = false
        self.otto.physicsBody?.isDynamic = false
        self.otto.physicsBody?.categoryBitMask = CollisionCategories.otto
        self.otto.physicsBody?.contactTestBitMask = CollisionCategories.Player
        
        // set values labels
        self.scoreScreen.text = "\(self.score)"
        self.livesScreen.text = "\(self.lives)"
        
        // implement the vector math algorithm to move zombie to person
        // enemy 1
        var a = player.position.x - self.enemy.position.x // (x2-x1)
        var b = player.position.y - self.enemy.position.y // (y2-y1)
        var h = sqrt((a*a) + (b*b))             // calculate hypotenuse
        
        self.xn1 = a/h           // xn = global class variable
        self.yn1 = b/h           // yn = global class variable
        
        // enemy 2
         a = player.position.x - self.enemy2.position.x // (x2-x1)
         b = player.position.y - self.enemy2.position.y // (y2-y1)
         h = sqrt((a*a) + (b*b))             // calculate hypotenuse
        
        self.xn2 = a/h           // xn = global class variable
        self.yn2 = b/h           // yn = global class variable
        
        // enemy 3
         a = player.position.x - self.enemy3.position.x // (x2-x1)
         b = player.position.y - self.enemy3.position.y // (y2-y1)
         h = sqrt((a*a) + (b*b))             // calculate hypotenuse
        
        self.xn3 = a/h           // xn = global class variable
        self.yn3 = b/h           // yn = global class variable
        
        if (self.enemy.frame.intersects(self.player.frame)){
            //print("DEU CERTO")
            self.lives = self.lives - 1
            
            if (self.lives == 0){
                self.gameOver()
            }else{
                //self.enemy.physicsBody = nil
                self.enemy.removeAllChildren()
                self.enemy.removeFromParent()
                self.livesScreen.text = "\(self.lives)"
                self.player.position.x = 206.691
                self.player.position.y = 438.586
            }
            
        }
    }
    
    func didBegin(_ contact: SKPhysicsContact) {
        
        print("entrei")
        var firstBody: SKPhysicsBody
        var secondBody: SKPhysicsBody
        if contact.bodyA.categoryBitMask < contact.bodyB.categoryBitMask {
            firstBody = contact.bodyA
            secondBody = contact.bodyB
        } else {
            firstBody = contact.bodyB
            secondBody = contact.bodyA
        }
        
        print("first = \(firstBody.node?.name)")
        print("second = \(secondBody.node?.name)")
        /*if ((firstBody.categoryBitMask & CollisionCategories.Enemy != 0) &&
            (secondBody.categoryBitMask & CollisionCategories.PlayerBullet != 0)){
            print("Enemy and Player Bullet Conatact")
            if let monster = firstBody.node as? SKSpriteNode,
                let projectile = secondBody.node as? SKSpriteNode {
                projectileDidCollideWithMonster(projectile: projectile, monster: monster)
            }
        }*/
        
        if (firstBody.node?.name == "orange" && secondBody.node?.name == "wall1") {
            firstBody.node?.removeAllChildren()
            firstBody.node?.removeFromParent()
            print("wall 1")
        }
        if (firstBody.node?.name == "orange" && secondBody.node?.name == "wall2") {
            firstBody.node?.removeAllChildren()
            firstBody.node?.removeFromParent()
            print("wall 2")
        }
        if (firstBody.node?.name == "orange" && secondBody.node?.name == "enemy") {
            firstBody.node?.removeAllChildren()
            firstBody.node?.removeFromParent()
            self.enemy.removeAllChildren()
            self.enemy.removeFromParent()
            self.score = self.score + 50
            self.scoreScreen.text = "\(self.score)"
        }
        if (firstBody.node?.name == "orange" && secondBody.node?.name == "enemy2") {
            firstBody.node?.removeAllChildren()
            firstBody.node?.removeFromParent()
            self.enemy2.removeAllChildren()
            self.enemy2.removeFromParent()
            self.score = self.score + 50
            self.scoreScreen.text = "\(self.score)"
        }
        if (firstBody.node?.name == "orange" && secondBody.node?.name == "enemy3") {
            firstBody.node?.removeAllChildren()
            firstBody.node?.removeFromParent()
            self.enemy3.removeAllChildren()
            self.enemy3.removeFromParent()
            self.score = self.score + 50
            self.scoreScreen.text = "\(self.score)"
        }
        if (firstBody.node?.name == "player" && secondBody.node?.name == "otto") {
            gameOver()
        }
        
    }
    
    func projectileDidCollideWithMonster(projectile: SKSpriteNode, monster: SKSpriteNode) {
        print("Hit")
        projectile.removeFromParent()
        monster.removeFromParent()
    }

    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        guard let touch = touches.first  else {
            return
        }
        
        let mouseLocation = touch.location(in: self)
        print("Finger starting position: \(mouseLocation)")
        
        // implement the vector math algorithm to move zombie to person
        // enemy 1
        var a = player.position.x - self.enemy.position.x // (x2-x1)
        var b = player.position.y - self.enemy.position.y // (y2-y1)
        var h = sqrt((a*a) + (b*b))             // calculate hypotenuse
        
        self.xn1 = a/h           // xn = global class variable
        self.yn1 = b/h           // yn = global class variable
        
        // enemy 2
        a = player.position.x - self.enemy2.position.x // (x2-x1)
        b = player.position.y - self.enemy2.position.y // (y2-y1)
        h = sqrt((a*a) + (b*b))             // calculate hypotenuse
        
        self.xn2 = a/h           // xn = global class variable
        self.yn2 = b/h           // yn = global class variable
        
        // enemy 3
        a = player.position.x - self.enemy3.position.x // (x2-x1)
        b = player.position.y - self.enemy3.position.y // (y2-y1)
        h = sqrt((a*a) + (b*b))             // calculate hypotenuse
        
        self.xn3 = a/h           // xn = global class variable
        self.yn3 = b/h           // yn = global class variable
        
        // Otto
        a = player.position.x - self.otto.position.x // (x2-x1)
        b = player.position.y - self.otto.position.y // (y2-y1)
        h = sqrt((a*a) + (b*b))             // calculate hypotenuse
        
        self.xnOtto = a/h           // xn = global class variable
        self.ynOtto = b/h           // yn = global class variable
        
        // detect what sprite was touched
        let spriteTouched = self.atPoint(mouseLocation)
        
        if (spriteTouched.name == "leftButton") {
            print("YOU CLICKED THE left button")
            if (self.player.xScale > 0){
                self.player.xScale = self.player.xScale * -1;
            }
            let girlLeft = self.player.position.x - self.SPEED;
            self.player.position = CGPoint(x: girlLeft, y: self.player.position.y)
            self.goingLeft = true
        }
        if (spriteTouched.name == "rightButton") {
            print("YOU CLICKED THE right button")
            if (self.player.xScale < 0){
                self.player.xScale = self.player.xScale * -1;
            }
            let girlRight = self.player.position.x + self.SPEED;
            self.player.position = CGPoint(x: girlRight, y: self.player.position.y)
            self.goingLeft = false
        }
        if (spriteTouched.name == "upButton") {
            print("YOU CLICKED THE up button")
            let girlUp = self.player.position.y + self.SPEED;
            self.player.position = CGPoint(x: self.player.position.x, y: girlUp)
        }
        if (spriteTouched.name == "downButton") {
            print("YOU CLICKED THE down button")
            let girlDown = self.player.position.y - self.SPEED;
            self.player.position = CGPoint(x: self.player.position.x, y: girlDown)
        }
        
        if (spriteTouched.name == "musicButton") {
            print("YOU CLICKED THE musica button")
            
            if (self.musicOn) {
                self.backgroundMusic.run(SKAction.stop())
                self.musicBtn.texture = SKTexture.init(imageNamed: "musicOff-light")
            }else{
                self.backgroundMusic.run(SKAction.play())
                self.musicBtn.texture = SKTexture.init(imageNamed: "musicOn-light")
            }
            self.musicOn = !self.musicOn
        }
        
        if (spriteTouched.name == "bButton") {
            fireBullet()
        }
        
        if (self.player.frame.intersects(self.enemy.frame)){
            //print("DEU CERTO")
            self.lives = self.lives - 1
            
            if (self.lives == 0){
                self.gameOver()
            }else{
                //self.enemy.physicsBody = nil
                self.enemy.removeAllChildren()
                self.enemy.removeFromParent()
                self.livesScreen.text = "\(self.lives)"
                
                if(currentLevel == 1){
                    self.player.position.x = 206.691
                    self.player.position.y = 438.586
                }else{
                    self.player.position.x = 258.691
                    self.player.position.y = 131.034
                }
            }
            
        }
        
        if (self.player.frame.intersects(self.enemy2.frame)){
            //print("DEU CERTO")
            self.lives = self.lives - 1
            
            if (self.lives == 0){
                self.gameOver()
            }else{
                //self.enemy.physicsBody = nil
                self.enemy2.removeAllChildren()
                self.enemy2.removeFromParent()
                self.livesScreen.text = "\(self.lives)"
                if(currentLevel == 1){
                    self.player.position.x = 206.691
                    self.player.position.y = 438.586
                }else{
                    self.player.position.x = 258.691
                    self.player.position.y = 131.034
                }
            }
            
        }
        
        if(currentLevel > 1){
            if (self.player.frame.intersects(self.enemy3.frame)){
                //print("DEU CERTO")
                self.lives = self.lives - 1
                
                if (self.lives == 0){
                    self.gameOver()
                }else{
                    //self.enemy.physicsBody = nil
                    self.enemy3.removeAllChildren()
                    self.enemy3.removeFromParent()
                    self.livesScreen.text = "\(self.lives)"
                    self.player.position.x = 258.691
                    self.player.position.y = 131.034
                }
                
            }
        }
        
        if (self.player.frame.intersects(self.exit.frame)){
            //print("DEU CERTO")
            self.gameWin()
        }
        
        if (self.player.frame.intersects(self.wall2.frame)){
            print("DEU CERTO 2")
            //self.gameWin()
        }
        
    }
    
    func fireBullet() {
        
        let bullet = SKSpriteNode(imageNamed: "Orange")
        bullet.setScale(1.1)
        bullet.position = player.position
        bullet.zPosition = 1
        bullet.physicsBody = SKPhysicsBody(rectangleOf: bullet.size)
        bullet.physicsBody!.affectedByGravity = false
        bullet.physicsBody!.categoryBitMask = CollisionCategories.PlayerBullet
        bullet.physicsBody!.collisionBitMask = CollisionCategories.Enemy + CollisionCategories.wall
        bullet.physicsBody!.contactTestBitMask = CollisionCategories.Enemy + CollisionCategories.wall
        bullet.name = "orange"
        addChild(bullet)
        
        //let soundAction = SKAction.playSoundFileNamed("bulletsSound.wav", waitForCompletion: true)
        var moveBullet = SKAction.moveTo(x: self.size.width + bullet.size.width, duration: 1)
        if (self.goingLeft) {
            moveBullet = SKAction.moveTo(x: (self.size.width + bullet.size.width) * -1, duration: 1)
        }
        
        let deleteBullet = SKAction.removeFromParent()
        let bulletSequce = SKAction.sequence([moveBullet, deleteBullet])
        bullet.run(bulletSequce)
        
        
    }
    
    override func touchesMoved(_ touches: Set<UITouch>, with event: UIEvent?) {
       
    }
    
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        guard let touch = touches.first  else {
            return
        }
        
        let mouseLocation = touch.location(in: self)
        print("Finger ending position: \(mouseLocation)")
        
        // 1. get the ending position of the finger
        let orangeEndingPosition = mouseLocation
        
        // 2. get the difference between finger start & end
        let diffX = enemy.position.x/*orangeEndingPosition.x*/ - self.mouseStartingPosition.x
        let diffY = enemy.position.y/*orangeEndingPosition.y*/ - self.mouseStartingPosition.y
        
        // 3. throw the orange based on that direction
        let direction = CGVector(dx: diffX, dy: diffY)
        self.orange?.physicsBody?.isDynamic = true
        self.orange?.physicsBody?.applyImpulse(direction)
        
        
        // 5. remove the line form the drawing
        //self.lineNode.path = nil
    }
    
    override func update(_ currentTime: TimeInterval) {
        // Called before each frame is rendered
        
        // Initialize _lastUpdateTime if it has not already been
        if (self.lastUpdateTime == 0) {
            self.lastUpdateTime = currentTime
        }

        // Calculate time since last update
        let dt = currentTime - self.lastUpdateTime
    
        // HINT: This code prints "Hello world" every 5 seconds
        if (dt > 30) {
            //print("HELLO WORLD!")
            self.lastUpdateTime = currentTime
            if (self.ottoYes == false){
                self.ottoYes = true
                self.createOtto()
            }
        }
        
        self.makeEnemyMove()
        
        if (self.enemy.frame.intersects(self.player.frame)){
            //print("DEU CERTO")
            self.lives = self.lives - 1
            
            if (self.lives == 0){
                self.gameOver()
            }else{
                self.enemy.physicsBody = nil
                self.enemy.removeAllChildren()
                self.enemy.removeFromParent()
                self.enemy.removeAllActions()
                self.livesScreen.text = "\(self.lives)"
                self.player.position.x = 206.691
                self.player.position.y = 438.586
            }
            
        }
    }
    
    func createOtto(){
        self.otto.texture = SKTexture.init(imageNamed: "otto")
        self.otto.name = "otto"
    }
    
    func makeEnemyMove(){
        //enemy 1
        self.enemy.position.x = self.enemy.position.x + self.xn1 * ENEMY_SPEED
        self.enemy.position.y = self.enemy.position.y + self.yn1 * ENEMY_SPEED
        
        // 1. check against left and right sides
        if (self.enemy.position.x <= 0 ||
            self.enemy.position.x >= self.size.width) {
            self.xn1 = self.xn1 * -1
        }
        
        // 2. check top and bottom of screen
        if (self.enemy.position.y <= 0 ||
            self.enemy.position.y >= self.size.height) {
            self.yn1 = self.yn1 * -1
        }
        
        //enemy 2
        self.enemy2.position.x = self.enemy2.position.x + self.xn2 * ENEMY_SPEED
        self.enemy2.position.y = self.enemy2.position.y + self.yn2 * ENEMY_SPEED
        
        // 1. check against left and right sides
        if (self.enemy2.position.x <= 0 ||
            self.enemy2.position.x >= self.size.width) {
            self.xn2 = self.xn2 * -1
        }
        
        // 2. check top and bottom of screen
        if (self.enemy2.position.y <= 0 ||
            self.enemy2.position.y >= self.size.height) {
            self.yn2 = self.yn2 * -1
        }
        
        //enemy 3
        if(currentLevel > 1){
            self.enemy3.position.x = self.enemy3.position.x + self.xn3 * ENEMY_SPEED
            self.enemy3.position.y = self.enemy3.position.y + self.yn3 * ENEMY_SPEED
            
            // 1. check against left and right sides
            if (self.enemy3.position.x <= 0 ||
                self.enemy3.position.x >= self.size.width) {
                self.xn3 = self.xn3 * -1
            }
            
            // 2. check top and bottom of screen
            if (self.enemy3.position.y <= 0 ||
                self.enemy3.position.y >= self.size.height) {
                self.yn3 = self.yn3 * -1
            }
        }
        
        if (ottoYes) {
            self.otto.position.x = self.otto.position.x + self.xnOtto * ENEMY_SPEED
            self.otto.position.y = self.otto.position.y + self.ynOtto * ENEMY_SPEED
            
            // 1. check against left and right sides
            if (self.otto.position.x <= 0 ||
                self.otto.position.x >= self.size.width) {
                self.xnOtto = self.xnOtto * -1
            }
            
            // 2. check top and bottom of screen
            if (self.otto.position.y <= 0 ||
                self.otto.position.y >= self.size.height) {
                self.ynOtto = self.ynOtto * -1
            }
        }
    }
    
    // MARK: Custom GameScene Functions
    // Your custom functions can go here
    // -------------------------------------
    
    func setLevel(levelNumber: Int){
        self.currentLevel = levelNumber
    }
    
    func gameOver(){
        let message = SKLabelNode(text:"GAME OVER")
        message.position = CGPoint(x:self.size.width/2, y:self.size.height/2)
        message.fontColor = UIColor.red
        message.fontSize = 100
        message.fontName = "AvenirNext-Bold"
        
        addChild(message)
        
        
        //RESTART
        // load the next level
        self.currentLevel = 1
        
        // try to load the next level
        guard let nextLevelScene = GameScene.loadLevel(levelNumber: self.currentLevel) else {
            print("Error when loading next level")
            return
            
        }
        
        // wait 2 second then switch to next leevl
        let waitAction = SKAction.wait(forDuration:2)
        
        let showNextLevelAction = SKAction.run {
            self.score = 0
            self.scoreScreen.text = "\(self.score)"
            self.lives = 2
            self.livesScreen.text = "\(self.lives)"
            nextLevelScene.setLevel(levelNumber: self.currentLevel)
            let transition = SKTransition.flipVertical(withDuration: 2)
            nextLevelScene.scaleMode = .aspectFill
            self.scene?.view?.presentScene(nextLevelScene, transition:transition)
        }
        
        let sequence = SKAction.sequence([waitAction, showNextLevelAction])
        
        self.run(sequence)
    }
    
    func gameWin() {
        self.levelComplete = true
        
        let message = SKLabelNode(text:"LEVEL \(self.currentLevel) COMPLETE!")
        message.position = CGPoint(x:self.size.width/2, y:self.size.height/2)
        message.fontColor = UIColor.magenta
        message.fontSize = 100
        message.fontName = "AvenirNext-Bold"
        
        addChild(message)
        
        // load the next level
        self.currentLevel = self.currentLevel + 1
        
        // try to load the next level
        guard let nextLevelScene = GameScene.loadLevel(levelNumber: self.currentLevel) else {
            print("Error when loading next level")
            return
            
        }
        
        // wait 1 second then switch to next leevl
        let waitAction = SKAction.wait(forDuration:1)
        
        let showNextLevelAction = SKAction.run {
            self.score = self.score + 50
            self.scoreScreen.text = "\(self.score)"
            nextLevelScene.setLevel(levelNumber: self.currentLevel)
            let transition = SKTransition.flipVertical(withDuration: 2)
            nextLevelScene.scaleMode = .aspectFill
            self.scene?.view?.presentScene(nextLevelScene, transition:transition)
        }
        
        let sequence = SKAction.sequence([waitAction, showNextLevelAction])
        
        self.run(sequence)
    }
    
    class func loadLevel(levelNumber:Int) -> GameScene? {
        let fileName = "BerzerkLevel\(levelNumber)"
        
        // DEBUG nonsense
        print("Trying to load file: \(fileName).sks")
        
        guard let scene = GameScene(fileNamed: fileName) else {
            print("Cannot find level named: \(fileName).sks")
            return nil
        }
        
        scene.scaleMode = .aspectFill
        return scene
        
    }
}
